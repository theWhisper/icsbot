__author__ = 'yonney'
import getpass
import sys
import telnetlib
import string
import Talk
import datetime
import arrow
from dateutil import parser

def main():
    HOST = "freechess.org"
    user = "Notify"
    password = "animal"
    tn = telnetlib.Telnet(HOST)

    tn.read_until("login: ")
    tn.write(user + "\n")
    if password:
        tn.read_until("password: ")
        tn.write(password + "\n")
    tn.write("set tell 1\n")
    tn.write("set pin 0\n")
    tn.write("set gin 0\n")
    tn.write("set seek 0\n")
    tn.write("tell yonney I am online! Lock & Loaded!\n")

    connection = 4
    #main loop
    print tn
    now = arrow.now().strftime("%Y-%m-%d %H:%M:%S")
    future_time = now

    while tn:
        player = tn.read_until("\n")
        letters = string.printable
        commands = ["HELP","REGISTER","UNREGISTER","SAVE","MESSAGES","OBEY_ME","SET","alarm"]

        print repr(player)
        if "tells you: REGISTER" in player:
            register = Talk.registration(player)
            tn.read_eager()
            tn.write("tell " + register + " Okay, you've been registered!\n")
            tn.write("tell " + register + ' Type "tell notify HELP" for all commands available\n')
            tn.read_eager()

    #tn.write("tell " + Talk.who(tell) + " \n")

        #player = tn.read_until("tells you: help")
        elif "tells you: HELP" in player:
            whois = Talk.who(player)
            help = str(Talk.com_help())
            help = help.rstrip('\n')
            tn.write("tell " + whois + " Hi, this is my help file! FOLLOW THE COMMAND RULES\n")
            tn.write("qtell " + whois + " " + help+ "\n")
            print "this is whois " + repr(whois)
            print "this is help " + repr(help)

        elif "tells you: UNREGISTER" in player:
            unregister = Talk.unregistration(player)
            tn.read_eager()
            tn.write("tell " + unregister + " Okay, you've been unregistered. Hope you come back soon!\n")


        elif "tells you: SAVE" in player:
            raw_player = player
            save = Talk.save_msg(player)
            nickname = Talk.who(raw_player)
            tn.read_eager()
            tn.write("tell " + nickname + str(save) + '\n')

        elif "tells you: MESSAGE" in player:
            name = Talk.who(player)
            print "this is name " + repr(name)
            message = Talk.com_show_msg(name)
            print "this is message " + repr(message)
            tn.read_eager()
            tn.write("tell " + name + message)

            #tn.write("tell " + register + " hey\n")
            #tn.write("tell " + register + " sup man\n")

        elif "tells you: OBEY_ME" in player:
            obey = player
            user = Talk.who(player)
            print "this is name " + repr(user)
            my_command = Talk.com_obey(user, obey)
            print "this is message " + repr(my_command)
            tn.read_eager()
            tn.write(my_command + "\n")

            #tn.write("tell " + register + " hey\n")
            #tn.write("tell " + register + " sup man\n")

        elif "tells you: SET" in player:
            raw_string = player # just to save the raw_line received.
            msg = raw_string #passing msg to set_alarm()
            user = Talk.who(player)
            time = raw_string #passing time to set_alarm()
            #time= Talk.get_time(time)
            alarm = Talk.set_alarm(msg, user, time)
            tn.write(alarm)
            print "this is name " + repr(user)
            print "this is time " + repr(time)
            print "this is message " + repr(msg)
            print "this is alarm " + repr(alarm)
            tn.read_eager()

        #avoiding blik replys
        # elif "blik(C) shouts:" in player:
        #     blik = Talk.blik_answer()
        #     tn.write("shout " + blik)


        # elif future_time == arrow.now():
        #     alarm = Talk.alarm()
        #     for last_message, name_id, alarm_time in alarm:
        #         message = str(last_message)
        #         player = str(name_id)
        #         alarms = str(alarm_time)
        #         print "this is weird " + repr(alarms)
        #         tn.write("tell %s Alarm bell: %s\n" % (player, message))
        #     future_time = arrow.now() + datetime.timedelta(minutes=1)
        #     print "this is futime" + repr(future_time)
        #     print "this is locatime" + repr(arrow.now())
        #     tn.read_eager

        #respond to anything that is not a command. This is a modified else clause - So proud of this function()
        elif any("tells you: %s" % errors in player for errors in letters if errors not in commands):
            r_mess = Talk.random_mess()
            name = Talk.who(player)
            print "this is name " + repr(name)
            tn.write("tell " + name + r_mess)
            tn.write("qtell " + name + ' Common Errors are: \\n      1. A typo in the command E.g: tell Notify help instead of "tell notify HELP"\\n '
                                       '     2. A case mismatch (all commands MUST BE in Uppercase)'
                                       ' E.g. HELP\\n'
                                       "      3. You must not leave more than 1 space when sending a command after my name\\n"
                                       "         For example: 'tell Notify HELP' is 'OK' but 'tell Notify  HELP' is 'NOT OK'. \\n"
                                       "         *NOTICE the single space ^      *NOTICE the extra space ^^\n")
            tn.read_eager()




    tn.write("quit\n")