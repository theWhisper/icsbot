__author__ = 'yonney'
import sqlite3
import random
import arrow
from dateutil import parser
import datetime

def who(tell):
    """
	(str) => str
	Receives a tell and detect the user's username
	:rtype : object
	"""
    lines = tell.splitlines()
    last_line = lines[-1]
    words = last_line.split()
    whois = words[0]
    whois = whois.split('(')[0]
    return whois

def brute(badpass, okpass):
    badpass = "password"
    okpass = "Present company"
    return None

def createList():
    charslist = "abcdefghijklmnopqrstuvwxyz_0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ /" \
                "`~!@#$%^&()_+}{=-\":?></.,|\\"









def get_time(time):
    #time = " ".join(time)
    # last_msg = "".join(last_msg)
    # print " this is pre user" + repr(user)
    #user = who(user)  # get user's name using who()
    try:
        time = time.splitlines()
        time = time[-1]
        time = time.split()
        time = time[4]
        print "this is time" + repr(time)
        time = parser.parse(time).strftime("%Y-%m-%d %H:%M")
        return time
    except (ValueError, IndexError):
        print "this is time" + repr(time)
        return None



# ALARM FUNCTION
def set_alarm(msg, user, time):
    """
	(int)->string
	Receives an integer in date format
	to set a alarm and returns a string
	saying the set time.
	"""
    time = get_time(time)
    error_raised_is = " Oops! I might miss that reminder.  Try again. Use this example: " \
                      "tell notify SET jan,1,2015,3:19 This is new year date"
    msg = msg
    lines = msg.splitlines()
    raw_msg = lines[-1]
    list = raw_msg.split()
    last_msg = list[5:]
    last_msg = " ".join(last_msg)
    print "this is the length" + repr(len(last_msg))

    if time is None:
        return "tell %s %s\n" % (user, error_raised_is)

    elif time <= arrow.now().strftime("%Y-%m-%d %H:%M"):

        return "tell %s You will miss this alarm because is in the past. Try a distant future.\n" % user

    elif len(last_msg) < 5:
        return "tell %s Sorry, your alarm message was too short. Must be more than 5 characters.\n" % user

    elif len(last_msg) > 37:
        return "tell %s Sorry, alarm message Must be less than 37 characters.\n" \
               % user

    else:
        con = sqlite3.connect('notifications.db')
        cur = con.cursor()
        cur.execute('INSERT INTO alarm(LAST_MESSAGE, NAME_ID, ALARM) VALUES (?,?,?);', (last_msg, user, time))
        con.commit()
        alarm = "tell %s Alarm has been set for %s\n" % (user, time)
        return alarm
    #fics_time == time.now().strftime("%Y, %m, %d, %H, %M") will use this as comparison



# ALARM FUNCTION
def alarm():
    """
	(none)->string
	in Alarm table that has ID, SET_ALARM, NAME_ID
	Verify players alarm and return
	alarm if current system time is == to alarmed set.
	saying the set time.
	"""
    #alarm = arrow.now().strftime("%Y, %m, %d, %H, %M") #will use this as comparison
    trig_alarm = arrow.now().strftime("%Y-%m-%d %H:%M")
    print "this is trig_alarm" + repr(trig_alarm)
    con = sqlite3.connect('notifications.db')
    cur = con.cursor()
    cur.execute('SELECT LAST_MESSAGE, NAME_ID, ALARM FROM alarm WHERE ALARM = ? ORDER BY ID DESC LIMIT 2', [trig_alarm])
    set_of_alarms = cur.fetchall()
    #for alarms in alarm:
    if not alarm:
        print "this is weird " + repr(alarm)
        pass
    #lhs, sep, rhs = message.partition("tells you: SAVE ")
    # message.partition("tells you: save")
    # message = message[2]
    else:
        print "this is fetchall" + repr(set_of_alarms)
        return set_of_alarms


# JUST A DUMMY FUNCTION I WAS USING TO TEST RESULTS USE WHO() INSTEAD
def real(str):
    tell = str
    lines = tell.splitlines()
    last_line = lines[-1]
    words = last_line.split()
    real_name = words[0]
    slic = real_name.index('(')
    if "(" in real_name:
        real_name = real_name[:slic]


# SCHEMA
def create_db():
    conn = sqlite3.connect('notifications.db')
    conn.execute('''CREATE TABLE users
	(ID INTEGER PRIMARY KEY,
	NAME           TEXT    NOT NULL);'''),

    conn.execute('''CREATE TABLE messages
	(ID INTEGER PRIMARY KEY,
	LAST_MESSAGE     TEXT     NOT NULL,
	NAME_ID          INT      NOT NULL);'''),

    conn.execute('''CREATE TABLE alarm
	(ID INTEGER PRIMARY KEY,
	LAST_MESSAGE     TEXT     NOT NULL,
	NAME_ID          INT      NOT NULL,
	ALARM            DATETIME NOT NULL);''')
    print "Database (notifications) and Table (users, messages, alarm) were created successfully!\n"


# REGISTER A PLAYER
def registration(register):
    """(string)->string
	Gets the player name and register
	the player into database notifications
	table users.
	:param whois:
	"""
    # register = tn.read_until("tells you: register")
    register = who(register)
    print "this is register " + repr(register)
    con = sqlite3.connect('notifications.db')
    cur = con.cursor()
    cur.execute('INSERT INTO users(NAME) VALUES (?);', (register,))
    con.commit()
    return register


# tn.write( "tell " + register + " Okay, you've been registered! Type help for a series of commands\n")


# SHOWS ALL COMMANDS AVAILABLE
def com_help():
    """ () ->str
	Simple function for reading from help file.
	It returns the file content.
	:return: lines
	"""
    file = open("commands.txt", "r")
    lines = file.read()
    file.close()
    return lines


# UN REGISTER A PLAYER
def unregistration(unregister):
    """(string)->string
	Gets the player name and unregister
	the player from database notifications
	table users.
	:param whois:
	"""
    # register = tn.read_until("tells you: register")
    unregister = who(unregister)
    print "this is register " + repr(unregister)
    con = sqlite3.connect('notifications.db')
    cur = con.cursor()
    cur.execute('DELETE FROM users WHERE NAME = ?', (unregister,))
    con.commit()
    return unregister


# CHECK WORDS IN A STRING
def is_valid_word(word, hand, wordList):
    """
	Returns True if word is in the wordList and is entirely
	composed of letters in the hand. Otherwise, returns False.

	Does not mutate hand or wordList.

	word: string
	hand: dictionary (string -> int)
	wordList: list of lowercase strings
	"""
    # TO DO ... <-- Remove this comment when you code this function
    answer = ""
    for letter in word:
        if wordList.count(word) > 0 and letter in hand and hand[letter] >= word.count(letter):
            answer = True
        else:
            answer = False
    return answer


# SAVE USER LAST TELL
def save_msg(msg):
    """
	(string)->string
	Get user message and saves it into database.
	:param msg:
	"""
    user = msg
    lines = msg.splitlines()
    raw_msg = lines[-1]
    list = raw_msg.split()
    last_msg = list[4:]
    last_msg = " ".join(last_msg)

    # last_msg = "".join(last_msg)
    print " this is pre user" + repr(user)

    user = who(user)  # get user's name using who()
    print "this is user" + repr(user)
    print "this is msg " + repr(last_msg)

    if last_msg == '':
        empty = " I cannot SAVE an empty message!"
        return empty
    else:
        con = sqlite3.connect('notifications.db')
        cur = con.cursor()
        cur.execute('INSERT INTO messages(LAST_MESSAGE, NAME_ID) VALUES (?,?);', (last_msg, user))
        con.commit()
        success = ' Okay message saved. Type "tell Notify MESSAGES" to see your last message'
        return success


# SHOW THE LAST MESSAGE OF USER
def com_show_msg(user):
    """
	()->str
	shows the last message from the user who ask for it
	:return:
	"""
    con = sqlite3.connect('notifications.db')
    cur = con.cursor()
    cur.execute('SELECT LAST_MESSAGE FROM messages WHERE NAME_ID = ? ORDER BY ID DESC LIMIT 1', [user])
    message = cur.fetchall()
    if not message:
        print "this is weird " + repr(message)
        message = " You have no messages. Use SAVE command to add messages\n"
        return message
    #lhs, sep, rhs = message.partition("tells you: SAVE ")
    # message.partition("tells you: save")
    # message = message[2]
    else:
        [[message]] = message
        print "this is weird " + repr(message)
        return " You Last Message Was: " + str(message) + "\n"  # [16:] # rhs == right hand string
                                                                # giving the result message I didn't like to mess with indexes.


# TAKE CONTROL OVER THE BOT TO SPOOF COMMAND
def com_obey(user, command):
    """
	(str)-> str
	Receives and string which is a command and issue
	the command to the server.
	This is only to take control over the bot "spoof command"
	:param command:
	:return None:
	"""
    command = command.splitlines()
    raw_msg = command[-1]
    list = raw_msg.split()
    last_msg = list[4:]
    last_msg = " ".join(last_msg)
    # last_msg = "".join(last_msg)
    # print " this is pre user" + repr(user)
    #user = who(user)  # get user's name using who()
    if user == 'Yonney':
        return last_msg
    else:
        denied = "tell %s You're not my boss... Anyway, you have insufficient rights" % user
        return denied


# A MUCH MORE BETTER WAY TO USE THE ALL-IN CONDITION TO TELLS YOU
def get_all():
    return_list = []
    for i in range(ord('A'), ord('Z') + 1):
        if chr(i) not in ['S', 'R', 'V', 'U', 'M']:
            return_list.append("tells you: %s" % chr(i))
    return return_list


# CHECKS ALL IN GET_ALL()
def contains(string):
    for i in get_all():
        if i in string:
            return True
    return False

#RANDOM MESSAGE FOR UNKNOWN COMMANDS
def random_mess():
    list = [" Easy!! mumble mouth. I Couldn't Understand You\n",
              " I getting so tired of people who do not read HELP FILE!\n",
              " You should look in HELP file you know? This is annoying\n",
              " Everytime people tell me something wrong I kill a 'NEURONE'\n",
              " Whatever! Here is the famous RULES! AS..... ASK ME AGAIN\n"]
    r_mess = random.choice(list)
    return r_mess

def blik_answer():
    list = [" Yeah whatever blik!\n",
              " Down came the spider and wipe the blik away!\n"
              " Hmm blik if you don't shut up now i will cut my vains, (circuits)\n",
              " No way blik, I must give you two of my neurones to improve you!\n",
              " blik if I were you, I'll check the mother board it looks faulty'\n",
              " blik come on EX-MACHINA your not AI you know? Stop pretending\n",
              " blik what would happen if I let water spill on you?\n",
              " hey bliky I have a question. Are you a boy or a girl?\n",
              " hey bliky whats your last name? blik _________?\n",
              " A round of applause to blik everybody. He reached 1500 elo\n"]
    r_mess = random.choice(list)
    return r_mess


#add a delimiter after 3 numbers
def formatter(number, delimiter):
    n = len(number)
    firstPartNumber = [number[:n%3]]
    number = [number[i:i+3] for i in range(n%3,n,3)]
    if n % 3:
        number = firstPartNumber + number

    return ",".join(number)

#this is the right solution to future time comparison - this cost me some enurones
def time():
    now = arrow.now().strftime("%Y-%m-%d %H:%M:%S")
    future_time = now
    i = 3
    while i == 3:

        if future_time == arrow.now().strftime("%Y-%m-%d %H:%M:%S"):
            now = arrow.now().strftime("%Y-%m-%d %H:%M:%S")
            future_time = arrow.now() + datetime.timedelta(minutes=1)
            future_time = future_time.strftime("%Y-%m-%d %H:%M:%S")

            print "this is now " + repr(now)

            print "this is futime" + repr(future_time)


def com_date():
    return arrow.now().strftime("%B, %d, %Y | Clock %I:%M:%S %p")